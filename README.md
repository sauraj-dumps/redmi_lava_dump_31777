## havoc_lava-userdebug 11 RQ3A.211001.001 1649560572 release-keys
- Manufacturer: xiaomi
- Platform: mt6768
- Codename: lava
- Brand: Redmi
- Flavor: havoc_lava-userdebug
- Release Version: 11
- Id: RQ3A.211001.001
- Incremental: 1649560572
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: false
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/coral/coral:12/SP2A.220305.012/8177914:user/release-keys
- OTA version: 
- Branch: havoc_lava-userdebug-11-RQ3A.211001.001-1649560572-release-keys
- Repo: redmi_lava_dump_31777


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
